FROM blang/latex:ctanfull

# Minted + Pygments
RUN tlmgr install minted
RUN apt-get update \
    && apt-get install -qy python python-pip \
    && pip install pygments \
    && rm -rf /var/lib/apt/lists/*
